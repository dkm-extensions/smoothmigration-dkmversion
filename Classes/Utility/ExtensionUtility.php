<?php
/**
 *  Copyright notice
 *
 *  ⓒ 2014 Michiel Roos <michiel@maxserv.nl>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is free
 *  software; you can redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by the Free Software Foundation;
 *  either version 2 of the License, or (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 */

/**
 * Class Tx_Smoothmigration_Utility_ExtensionUtility
 */
class Tx_Smoothmigration_Utility_ExtensionUtility implements t3lib_Singleton {

	/**
	 * @var null
	 */
	static $installedExtensions = NULL;

	/**
	 * @var null
	 */
	static $loadedExtensions = NULL;

	/**
	 * @var null
	 */
	static $loadedExtensionsFiltered = NULL;

	/**
	 * @var array
	 */
	static $packages = array();

	/**
	 * Current TYPO3 LTS version
	 */
	const CURRENT_LTS_VERSION = '6.2.0';

	/**
	 * Get extensions wich claim to be compatible in their ext_emconf.php
	 *
	 * Note that we are ignoring open ended comatibility here. These are the
	 * cases where the version requirements have a 0.0.0 at the end. This is
	 * because we assume that extension creators that care about compatibilty
	 * will specify the maximum supported version instead of providing a 0.0.0
	 * as upper limit.
	 *
	 * @param string $version The version to check against
	 * @param boolean $ignoreOpenEnd Should we ignore open ended requirements?
	 *
	 * @return array Array of compatible extension keys and their version ranges
	 */
	public static function getCompatibleExtensions($version = NULL, $ignoreOpenEnd = TRUE) {
		if ($version === NULL) {
			$version = self::CURRENT_LTS_VERSION;
		}
		$compatibleExtensions = array();
		$list = self::getInstalledExtensions(TRUE);
		foreach ($list as $extensionName => $extensionData) {
			if (isset($extensionData['EM_CONF']['constraints']['depends']['typo3'])) {
				$versionRange = self::splitVersionRange($extensionData);
				if ((bool)$ignoreOpenEnd) {
					$upperBound = $versionRange[1] !== '0.0.0' && version_compare($version, $versionRange[1], '<=');
				} else {
					$upperBound = $versionRange[1] === '0.0.0' || version_compare($version, $versionRange[1], '<=');
				}
				if (($versionRange[0] === '0.0.0' || version_compare($version, $versionRange[0], '>')) && $upperBound) {
					$compatibleExtensions[$extensionName] = $versionRange;
				}
			}
		}
		return $compatibleExtensions;
	}

	/**
	 * Get extensions wich do not claim to be compatible in their ext_emconf.php
	 *
	 * Note that we are ignoring open ended comatibility here. These are the
	 * cases where the version requirements have a 0.0.0 at the end. This is
	 * because we assume that extension creators that care about compatibilty
	 * will specify the maximum supported version instead of providing a 0.0.0
	 * as upper limit.
	 *
	 * @param string $version The version to check against
	 * @param boolean $ignoreOpenEnd Should we ignore open ended requirements?
	 *
	 * @return array Array of compatible extension keys and their version ranges
	 */
	public static function getIncompatibleExtensions($version = NULL, $ignoreOpenEnd = TRUE) {
		if ($version === NULL) {
			$version = self::CURRENT_LTS_VERSION;
		}
		$extensions = array();
		$list = self::getInstalledExtensions(TRUE);
		foreach ($list as $extensionName => $extensionData) {
//			list($extensionName, $extensionData) = self::getExtensionNameAndConfigurationData($extensionName, $extensionData);
			if (is_array($extensionData) && isset($extensionData['EM_CONF']['constraints']['depends']['typo3'])) {
				$versionRange = self::splitVersionRange($extensionData);
				if ((bool)$ignoreOpenEnd) {
					$upperBound = $versionRange[1] !== '0.0.0' && version_compare($version, $versionRange[1], '>');
				} else {
					$upperBound = $versionRange[1] === '0.0.0' || version_compare($version, $versionRange[1], '>');
				}
				if (($versionRange[0] !== '0.0.0' && version_compare($version, $versionRange[0], '<')) || $upperBound) {
					$extensions[$extensionName] = $versionRange;
				}
			}
		}
		return $extensions;
	}

	/**
	 * Get extensions that have category plugin or category fe
	 *
	 * @param boolean $onlyKeys If true, only the extension keys are returned.
	 *
	 * @return array Array of frontend extension keys
	 */
	public static function getFrontendExtensions($onlyKeys = TRUE) {
		$extensions = array();
		if (t3lib_div::int_from_ver(TYPO3_version) < 6001000) {
			$list = array();
			/** @var $extensionList tx_em_Extensions_List */
			$extensionList = t3lib_div::makeInstance('tx_em_Extensions_List');
			$cat = tx_em_Tools::getDefaultCategory();
			$path = PATH_typo3conf . 'ext/';
			$extensionList->getInstExtList($path, $list, $cat, 'L');
		} else {
//			$list = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getLoadedExtensionListArray();
			$list = self::getPackages();
		}
		foreach ($list as $extensionName => $extensionData) {
//			list($extensionName, $extensionData) = self::getExtensionNameAndConfigurationData($extensionName, $extensionData);
			if (isset($extensionData['EM_CONF']['category'])) {
				if ((trim($extensionData['EM_CONF']['category']) === 'plugin') ||
				    (trim($extensionData['EM_CONF']['category']) === 'fe')
				) {
					if ((bool)$onlyKeys) {
						array_push($extensions, $extensionName);
					} else {
						$extensions[$extensionName] = $extensionData;
					}
				}
			}
		}

		return $extensions;
	}

	/**
	 * Get a list of installed extensions
	 *
	 * @return array of installed extensions
	 */
	public static function getInstalledExtensions($returnExtensionData = FALSE) {
		if (self::$installedExtensions !== NULL) {
			return $returnExtensionData ? self::$installedExtensions : array_keys(self::$installedExtensions);
		}
		if (t3lib_div::int_from_ver(TYPO3_version) < 6001000) {
			/** @var $extensionList tx_em_Extensions_List */
			$extensionList = t3lib_div::makeInstance('tx_em_Extensions_List');
			list($list,) = $extensionList->getInstalledExtensions();
		} else {
//			$list = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getLoadedExtensionListArray();
			$list = self::getPackages();
		}
		self::$installedExtensions = $list;
		return $returnExtensionData ? $list : array_keys($list);
	}


	/**
	 * @param string $type
	 * @return array
	 * @throws \TYPO3\Flow\Package\Exception\InvalidPackageStateException
	 */
	public function getPackages($type='available') {
		$type = strtolower($type);
		$packages = array();
		$objectManager = t3lib_div::makeInstance('Tx_Extbase_Object_ObjectManager');
		/**@var $packageManager TYPO3\CMS\Core\Package\PackageManager*/
		$packageManager = $objectManager->get('TYPO3\\CMS\\Core\\Package\\PackageManager');
		$packages = $packageManager->getFilteredPackages($type);
		//get $EM_CONF array, the EM_CONF data is too incapsulated to get (\TYPO3\CMS\Core\Package\Package::getExtensionEmconf)
		/**@var $package TYPO3\CMS\Core\Package\Package*/
		foreach ($packages as $packageName => $package) {
			$_EXTKEY = $packageName;
			if($_EXTKEY) {
				include($package->getPackagePath() . '/ext_emconf.php');
				$GLOBALS['TYPO3_LOADED_EXT'][$packageName]['type'] = strpos($package->getPackagePath(),'typo3/sysext/') ? 'S' : 'L';
				self::$packages[$type][$packageName] = array('EM_CONF' => $EM_CONF[$_EXTKEY]);
			}
		}
		return self::$packages[$type];
	}

	/**
	 * Get a list of loaded / active extensions
	 *
	 * @return array Array of installed
	 */
	public static function getLoadedExtensions($returnExtensionData = FALSE) {
		if (self::$loadedExtensions !== NULL) {
			return $returnExtensionData ? self::$loadedExtensions : array_keys(self::$loadedExtensions);
		}
		$loadedExtensions = array();
		if (t3lib_div::int_from_ver(TYPO3_version) < 6002000) {
			$installedExtensions = self::getInstalledExtensions(TRUE);
			foreach ($installedExtensions as $key) {
				if (count($GLOBALS['TYPO3_LOADED_EXT'][$key]) > 2) {
					$loadedExtensions[] = $key;
				}
			}
		} else {
			$loadedExtensions = self::getPackages('active');
		}
		self::$loadedExtensions = $loadedExtensions;

		return $returnExtensionData ? $loadedExtensions : array_keys($loadedExtensions);
	}


		/**
	 * Get a filtered list of loaded / active extensions
	 *
	 * Compatible extensions can be filtered out.
	 * Ignored extensions can be filtered out.
	 * System extensions can be filtered out.
	 * Smoothmigration is filtered out.
	 *
	 * @param bool $removeCompatible
	 * @param bool $removeIgnored
	 * @param bool $removeSystem
	 *
	 * @return array Array of installed
	 */
	public static function getLoadedExtensionsFiltered(
		$removeCompatible = TRUE,
		$removeIgnored = TRUE,
		$removeSystem = TRUE
	) {
		if (self::$loadedExtensionsFiltered !== NULL) {
			return self::$loadedExtensionsFiltered;
		}

		// get extension configuration
		$configuration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['smoothmigration']);

		if (isset($configuration['includeInactiveExtensions']) &&
		    intval($configuration['includeInactiveExtensions']) > 0
		) {
			$loadedExtensionsFiltered = self::getInstalledExtensions();
		} else {
			$loadedExtensionsFiltered = self::getLoadedExtensions();
		}
		$loadedExtensionsFiltered = array_flip($loadedExtensionsFiltered);
		unset($loadedExtensionsFiltered['smoothmigration']);

		if ($removeCompatible) {
			if (isset($configuration['excludeCompatibleExtensions']) &&
			    intval($configuration['excludeCompatibleExtensions']) > 0
			) {
				$targetVersion = NULL;
				if (isset($configuration['targetVersionOverride']) &&
				    trim($configuration['targetVersionOverride'])
				) {
					$targetVersion = trim($configuration['targetVersionOverride']);
				}
				$compatibleExtensions = Tx_Smoothmigration_Utility_ExtensionUtility::getCompatibleExtensions($targetVersion);
				foreach ($compatibleExtensions as $key => $_) {
					unset($loadedExtensionsFiltered[$key]);
				}
			}
		}
		if ($removeIgnored) {
			if (isset($configuration['excludedExtensions']) &&
			    trim($configuration['excludedExtensions']) !== ''
			) {
				$ingoreExtensions = explode(',', str_replace(' ', '', $configuration['excludedExtensions']));
				foreach ($ingoreExtensions as $key) {
					unset($loadedExtensionsFiltered[$key]);
				}
			}
		}
		if ($removeSystem) {
			foreach ($loadedExtensionsFiltered as $key => $_) {
				if ($GLOBALS['TYPO3_LOADED_EXT'][$key]['type'] === 'S') {
					unset($loadedExtensionsFiltered[$key]);
				}
			}
		}
		$loadedExtensionsFiltered = array_flip($loadedExtensionsFiltered);
		self::$loadedExtensionsFiltered = $loadedExtensionsFiltered;

		return self::$loadedExtensionsFiltered;
	}

	/**
	 * Get extensions that are marked as obsolete in their ext_emconf.php
	 *
	 * @param boolean $onlyKeys If true, only the extension keys are returned.
	 *
	 * @return array Array of obsolete extension keys
	 */
	public static function getObsoleteExtensions($onlyKeys = TRUE) {
		$extensions = array();
		$list = self::getInstalledExtensions(TRUE);
		foreach ($list as $extensionName => $extensionData) {
//			list($extensionName, $extensionData) = self::getExtensionNameAndConfigurationData($extensionName, $extensionData);
			if (isset($extensionData['EM_CONF']['state'])) {
				if (trim($extensionData['EM_CONF']['state']) === 'obsolete') {
					if ((bool)$onlyKeys) {
						array_push($extensions, $extensionName);
					} else {
						$extensions[$extensionName] = $extensionData;
					}
				}
			}
		}

		return $extensions;
	}

	/**
	 * If $extensionDataOrKey array, then return. Else get the $EM_CONF array from ext_emconf.php from the key.
	 * @param $extensionName
	 * @param mixed $extensionDataOrKey
	 * @return array
	 */
	public static function getExtensionNameAndConfigurationData($extensionName, $extensionDataOrKey) {
		if (t3lib_div::int_from_ver(TYPO3_version) < 6002000) {
			return array($extensionName, $extensionDataOrKey);
		} else {
			$_EXTKEY = $extensionDataOrKey;
			$extensionConfigurationData = '';
			if($_EXTKEY) {
				$extensionAbsolutePath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY);
				include($extensionAbsolutePath . '/ext_emconf.php');
				$extensionConfigurationData = array('EM_CONF' => $EM_CONF[$_EXTKEY]);
			}
			return array($_EXTKEY, $extensionConfigurationData);
		}
	}

	/**
	 * @param $extensionData
	 * @return array
	 */
	public function splitVersionRange($extensionData) {
		if (t3lib_div::int_from_ver(TYPO3_version) < 6002000) {
			$versionRange = tx_em_Tools::splitVersionRange($extensionData['EM_CONF']['constraints']['depends']['typo3']);
		} else {
			$versionRange = \TYPO3\CMS\Core\Utility\VersionNumberUtility::splitVersionRange($extensionData['EM_CONF']['constraints']['depends']['typo3']);
		}
		return $versionRange;
	}

}